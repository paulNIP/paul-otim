package com.musala.thedrone.config;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.musala.thedrone.model.Drone;
import com.musala.thedrone.repository.DroneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Component
@RestController
public class BatteryAudit {

    @Autowired
    DroneRepository droneRepository;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
            "MM/dd/yyyy HH:mm:ss");

    Logger logger = LoggerFactory.getLogger(BatteryAudit.class);

    @Scheduled(fixedRate = 10000)
    @RequestMapping("/battery_levels")
    public String droneBatteryLevels() {

        System.out.println(""
                + dateFormat.format(new Date()));
        List<Drone> drones = new ArrayList<Drone>();


        droneRepository.findAll().forEach(drones::add);

        for (int i = 0; i < drones.size(); i++) {
            logger.info("Drone "+drones.get(i).getSerialNumber()+" is at "+drones.get(i).getBatteryCapacity()+"%");
        }

        return "Howdy! Check out the Logs to see the output...";


    }


}
