package com.musala.thedrone.util;

public enum DroneState {

    IDLE,
    LOADING,
    LOADED,
    DELIVERING,
    DELIVERED,
    RETURNING;
}
