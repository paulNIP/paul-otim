package com.musala.thedrone.util;

public enum DroneModel {
    Lightweight,
    Middleweight,
    Cruiserweight,
    Heavyweight;
}
