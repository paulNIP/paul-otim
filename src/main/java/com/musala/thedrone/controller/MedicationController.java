package com.musala.thedrone.controller;


import com.musala.thedrone.exception.ResourceNotFoundException;
import com.musala.thedrone.model.Drone;
import com.musala.thedrone.model.Medication;
import com.musala.thedrone.repository.DroneRepository;
import com.musala.thedrone.repository.MedicationRepository;
import com.musala.thedrone.util.ImageUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/api")

public class MedicationController {

    @Autowired
    private DroneRepository droneRepository;

    @Autowired
    private MedicationRepository medicationRepository;

    public String msg;


    @GetMapping("/drones/{droneId}/medications")
    public ResponseEntity<List<Medication>> getAllMedicationsByDroneId(@PathVariable(value = "droneId") Long droneId) {
        if (!droneRepository.existsById(droneId)) {
            throw new ResourceNotFoundException("Not found Drone with id = " + droneId);
        }

        List<Medication> medications = medicationRepository.findByDroneId(droneId);
        return new ResponseEntity<>(medications, HttpStatus.OK);
    }

    @GetMapping("/medications_by_drone/{id}")
    public ResponseEntity<Medication> getMedicationsByDroneId(@PathVariable(value = "id") Long id) {
        Medication medication = medicationRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Not found Medication for Drone with id = " + id));

        return new ResponseEntity<>(medication, HttpStatus.OK);
    }

    @PostMapping("/drones/{droneId}/create_medications")

    public ResponseEntity<ImageUploadResponse> createMedication(
                                                                @RequestParam("image") MultipartFile file,
                                                                @RequestParam("name") String name,
                                                                @RequestParam("weight") Integer weight,
                                                                @RequestParam("code") String code,
                                                                @PathVariable(value = "droneId") Long droneId)
            throws IOException {

       Medication medication = droneRepository.findById(droneId).map(drone-> {

           System.out.println(drone.getBatteryCapacity());
           // prevent drone from being loaded if battery capacity is less than 25



               try {
                   if(drone.getBatteryCapacity()<25){

                       msg="Can not Load Drone with battery level less than 25 %";


                   } else if (drone.getWeightLimit()<weight) {
                       //prevent drone from loading more than it can carry
                       msg="Can not Load Drone with weight more than it can carry ";


                   } else if (!drone.getState().equals("IDLE")) {
                       //prevent drone from loading more than it can carry
                       msg="Can only load IDLE drone ";

                   }

                   if(drone.getBatteryCapacity()>=25 && drone.getWeightLimit()>weight && drone.getState().equals("IDLE") ) {
                       msg = "Medication added successfully: ";
                       return medicationRepository.save(Medication.builder()
                               .imageName(file.getOriginalFilename())
                               .imageType(file.getContentType())
                               .name(name)
                               .weight(weight)
                               .code(code)
                               .drone(drone)
                               .image(ImageUtility.compressImage(file.getBytes())).build()
                       );
                   }else {
                       //Drone not found
                       msg="Drone not found ";

                   }


               } catch (IOException e) {
                   throw new RuntimeException(e);
               }


           return null;
       }).orElseThrow(() -> new ResourceNotFoundException(msg ));



        return ResponseEntity.status(HttpStatus.OK)
                .body(new ImageUploadResponse(msg +
                        file.getOriginalFilename()));
    }



    @PutMapping("/update_medication/{id}")
    public ResponseEntity<Medication> updateMedication(@PathVariable("id") long id, @RequestBody Medication medicationRequest) {
        Medication medication = medicationRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("MedicationId " + id + "not found"));

        medication.setName(medication.getName());
        medication.setWeight(medication.getWeight());
        medication.setCode(medication.getCode());

        return new ResponseEntity<>(medicationRepository.save(medication), HttpStatus.OK);
    }

    @DeleteMapping("/delete_medication/{id}")
    public ResponseEntity<HttpStatus> deleteMedication(@PathVariable("id") long id) {
        medicationRepository.deleteById(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/delete_medication_of_drone/{droneId}/medications")
    public ResponseEntity<List<Medication>> deleteAllMedicationsOfDrone(@PathVariable(value = "droneId") Long droneId) {
        if (!droneRepository.existsById(droneId)) {
            throw new ResourceNotFoundException("Not found Drone with id = " + droneId);
        }

        medicationRepository.deleteByDroneId(droneId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}