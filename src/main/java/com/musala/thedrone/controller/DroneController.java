package com.musala.thedrone.controller;

import com.musala.thedrone.exception.ResourceNotFoundException;
import com.musala.thedrone.model.Drone;
import com.musala.thedrone.repository.DroneRepository;
import com.musala.thedrone.util.DroneModel;
import com.musala.thedrone.util.DroneState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/api")


public class DroneController {
    @Autowired
    DroneRepository droneRepository;

    @GetMapping("/drones")
    public ResponseEntity<List<Drone>> getAllDrones(@RequestParam(required = false) String serialNumber) {
        List<Drone> drones = new ArrayList<Drone>();

        if (serialNumber == null)
            droneRepository.findAll().forEach(drones::add);
        else
            droneRepository.findBySerialNumberContaining(serialNumber).forEach(drones::add);

        if (drones.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(drones, HttpStatus.OK);
    }

    @GetMapping("/drones/{id}")
    public ResponseEntity<Drone> getDroneById(@PathVariable("id") long id) {
        Drone drone = droneRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Not found Drone with id = " + id));

        return new ResponseEntity<>(drone, HttpStatus.OK);
    }



    @PostMapping("/add_drone")
    public ResponseEntity<Drone> createDrone(@RequestBody Drone drone) {
        Drone _drone = droneRepository.save(new Drone(
                drone.getSerialNumber(),
                drone.getModel(),
                drone.getWeightLimit(),
                drone.getBatteryCapacity(),
                drone.getState()

        ));
        return new ResponseEntity<>(_drone, HttpStatus.CREATED);
    }

    @PutMapping("/update_drone/{id}")
    public ResponseEntity<Drone> updateDrone(@PathVariable("id") long id, @RequestBody Drone drone) {
        Drone _drone = droneRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Not found Drone with id = " + id));

        _drone.setSerialNumber(drone.getSerialNumber());
        _drone.setModel(drone.getModel());
        _drone.setWeightLimit(drone.getWeightLimit());
        _drone.setBatteryCapacity(drone.getBatteryCapacity());
        _drone.setState(drone.getState());

        return new ResponseEntity<>(droneRepository.save(_drone), HttpStatus.OK);
    }

    @DeleteMapping("/delete_drone/{id}")
    public ResponseEntity<HttpStatus> deleteDrone(@PathVariable("id") long id) {
        droneRepository.deleteById(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/delete_all_drones")
    public ResponseEntity<HttpStatus> deleteAllTutorials() {
        droneRepository.deleteAll();

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

//    @GetMapping("/drones/published")
//    public ResponseEntity<List<Drone>> findByPublished() {
//        List<Drone> tutorials = droneRepository.findByPublished(true);
//
//        if (tutorials.isEmpty()) {
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        }
//
//        return new ResponseEntity<>(tutorials, HttpStatus.OK);
//    }
}
