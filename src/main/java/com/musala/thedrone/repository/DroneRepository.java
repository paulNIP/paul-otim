package com.musala.thedrone.repository;

import com.musala.thedrone.model.Drone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DroneRepository extends JpaRepository<Drone, Long> {


    List<Drone> findBySerialNumberContaining(String serialNumber);

    //available drones for loading
    List<Drone> findByStateContaining(String state);


}