package com.musala.thedrone.repository;

import com.musala.thedrone.model.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MedicationRepository extends JpaRepository<Medication, Long> {
    List<Medication> findByDroneId(Long droneId);

    @Transactional
    void deleteByDroneId(long droneId);
}