# paul-otim
There is a major new technology that is destined to be a disruptive force in the field of transportation: **the drone**. Just as the mobile phone allowed developing countries to leapfrog older technologies for personal communication, the drone has the potential to leapfrog traditional transportation infrastructure.

Useful drone functions include delivery of small items that are (urgently) needed in locations with difficult access.


## Getting started

This project built using Java and the following tools:

    Spring Boot as server side framework
    Maven as build automation tool
    Hibernate as ORM / JPA implementation
    H2 as database implementation
    Spring Data JPA as the top layer over Hibernate

(#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/paulNIP/paul-otim.git
git branch -M main
git push -uf origin main
```

## Application Structure

- Model

Domain model is organized under the model package and it consists of entity classes. Entities use various annotations that describe the relationships between each other. All these annotations are used by JPA in order to map entities to database tables.

-Repository

Repositories are interfaces that are responsible for data persistence and retrieval. The repository layer is an abstraction that provides all CRUD functionality and keeps hidden the data related information (e.g. specific database implmentation) from the other layers. This layer should always persist entities.

-Controller

Controller layer depends on the service layer and is responsible for the incoming requests and the outgoing responses. A controller determines all the available endpoints that client side (or other api) is able to call. This layer should not apply logic on the receiving or returning data.

## Build project

Build the application using the following maven wrapper command:

    ./mvnw clean package

In case you want to use this project as a dependency in other projects locally, use the following command:

    ./mvnw clean install

Each of these commands will create an executable .jar file at target directory.

## Run project
After packaging the application into an executable .jar file, you can start the server running the following command using any terminal in the project directory:

    java -jar target/paul-otim-0.0.1.jar

Alternatively, you can start the server without packaging, by running the following command:

    ./mvnw spring-boot:run

The server will start running at http://localhost:8080.



## After running
Every time the server is running, it triggers the flyway migration tool to look for possible schema changes. The first time, it will create a table called drone and medication, in order to write down all the scripts have already run, in a versioning way. Additionally, it will run all the available scripts at src/main/resources/ directory.

After the first run, springboot will detect and run only the new scripts.

## API Documentation
When server is up and running, you can use swagger to explore the available endpoints and try them out. Find it at: http://localhost:8080/swagger-ui/index.html

## License
This project is licensed under the terms of the MIT license. Check LICENSE file.

 keep going. You can also make an explicit request for maintainers.
